# Presentación de GitBook

## Origen

GitBook fue publicado como proyecto en el año 2014 bajo una licencia Apache 2.0, con el objetivo de conseguir una solución simple y moderna para la gestión de nuestra documentación. Está desarrollado con JavaScript como una librería de NodeJS, y puede ser usado desde la línea de comandos.

## Situación actual

Tal y como se indica en el [proyecto en Github](https://github.com/GitbookIO/gitbook), a día de hoy el equipo de desarrolladores original de este proyecto está trabajando en [Gitbook.com](https://www.gitbook.com/), por lo que el proyecto original ya no se está actualizando.

## Ventajas

* Los contenidos se guardan en un formato basado en texto plano, como es *markdown* o *asciidoc*, lo que facilita su tratamento de forma aislada, o con un editor externo diferente a GitBook.
* Exportación simple y rápida a otros formatos (HTML, PDF, ePUB...)
* Integración con un sistema de control de versiones. Publicación automática en la nube.
