# Exportar a otros formatos

## Requisitos previos

Para poder ejecutar los siguientes comandos tenemos que tener instalado el aplicativo [Calibre](https://calibre-ebook.com/), y así poder hacer uso de la utilidad `ebook-convert`.

## Conversión a ePub

	$ gitbook epub ./ ./mybook.epub

## Conversión a PDF

	$ gitbook pdf ./ ./mybook.pdf

## Conversión a Mobi

	$ gitbook mobi ./ ./mybook.mobi

