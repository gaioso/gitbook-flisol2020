# Índice de contenidos con DocToc

Con esta utilidad seremos capaces de crear de forma automática una tábla de contenidos en un documento con formato *markdown*. Tan solo tendremos que insertar las etiquetas correspondientes en el punto donde deseemos que aparezca dicho índice.

## Etiquetas

Por defecto, la tabla de contenidos se sitúa al comienzo del documento, y recoge los nombres de los títulos que aparecen en el resto de documento.

	<!-- START doctoc -->
	<!-- END doctoc -->

A continuación, ejecutamos el seguinte comando para que se añada de forma automática este índice:

	$ doctoc <fichero> --gitlab --title '## Índice de contenidos'

## Más información

[Repositorio en Github](https://github.com/thlorenz/doctoc)
