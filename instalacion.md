# Instalación de GitBook

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Indice de contenidos

- [GNU/Linux](#gnulinux)
- [Primer proyecto](#primer-proyecto)
- [Sitio estático](#sitio-estático)
- [Servidor local](#servidor-local)
- [Vídeos](#vídeos)
    - [Instalación de nodejs y npm](#instalaci%C3%B3n-de-nodejs-y-npm)
    - [Instalación de Gitbook y DocToc](#instalaci%C3%B3n-de-gitbook-y-doctoc)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GNU/Linux

Para proceder con la instalación de GitBook debemos ejecutar los siguientes comandos:

> Ubuntu 18.04 (bionic beaver)

	# apt install nodejs npm
	# npm install -g gitbook gitbook-cli doctoc

## Primer proyecto

Si queremos crear un primer proyecto vacío en el directorio actual:

	$ gitbook init

## Sitio estático

Para crear el contenido estático en formato HTML en un subdirectorio `_book`:

	$ gitbook build

## Servidor local

Para poder visualizar el contenido creado en el paso anterior, iniciamos un servidor local:

	$ gitbook serve
	...
	Starting server ...
	Serving book on http://localhost:4000

## Vídeos

### Instalación de nodejs y npm

<iframe src="https://archive.org/embed/nodejs_install" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

### Instalación de Gitbook y DocToc

<iframe src="https://archive.org/embed/gitbook_install" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

